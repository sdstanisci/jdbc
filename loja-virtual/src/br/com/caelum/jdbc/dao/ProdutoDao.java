package br.com.caelum.jdbc.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import br.com.caelum.jdbc.modelo.Produto;

public class ProdutoDao {
	
	private final Connection connection;
	
	public ProdutoDao(Connection connection){
		this.connection = connection;
	}
	
	public void listar() throws SQLException{
		
		Statement stmt = null;
		Boolean result = null;
		
		try{
				
			stmt = connection.createStatement();
			result = stmt.execute("select * from produto");
			
			ResultSet rs = stmt.getResultSet();
			System.out.println("Rsultado:" + result);
			
			if (result) {
				while(rs.next()){
					
				System.out.println("Id: " + rs.getInt("id"));
				System.out.println("Nome: " + rs.getString("nome"));
				System.out.println("Descri��o: " + rs.getString("descricao"));
				System.out.println("---------------------------------------");
				}
			}
		
		}catch(SQLException e){
			System.out.println("Erro ao consultar registro: ");
			e.printStackTrace();
			
			connection.rollback();
		}finally {
			System.out.println("Fechando conex�o.");
			stmt.close();
			connection.close();
		}
	}
	
	public void inserir(Produto produto) throws SQLException{
		
		PreparedStatement stmt = null;
		Boolean result = null;
		
		try{
			
			result = null;
			connection.setAutoCommit(false);
			
			String sql = "insert into Produto (nome, descricao) values (? , ?)";
			stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, produto.getNome());
			stmt.setString(2, produto.getDescricao());
			
			result = stmt.execute();
			System.out.println("Inserir - Result:" + result);
						
			ResultSet rs = stmt.getGeneratedKeys();
			rs.next();
			Integer id = Integer.parseInt(rs.getString("id")); 
			
			System.out.println("Id:" + id + " Inserido no Banco !");
			connection.commit();
			
		}catch(SQLException e){
			System.out.println("Erro ao inserir registro: ");
			e.printStackTrace();
			
			connection.rollback();
		}finally {
			System.out.println("Fechando conex�o.");
			stmt.close();
			connection.close();
		}
	}
	
	public void remover() throws SQLException{
		
		Statement stmt = connection.createStatement();
		stmt.execute(" delete from produto where id > 3 ");
		
		Integer count = stmt.getUpdateCount(); 
		
		System.out.println(count + " removidos do banco !");
		
		stmt.close();
		connection.close();
	}

}
