package br.com.caelum.jdbc.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import br.com.caelum.jdbc.dao.ConnectionPool;
import br.com.caelum.jdbc.modelo.Produto;

public class TestansercaoProduto {

	public static void main(String[] args) throws SQLException {
		
		Produto mesa = new Produto("Mesa", "Linda");
		
		Connection connection = null;
		PreparedStatement stmt = null;
		Boolean result = null;
		
		try{
			
			result = null;
			
			connection = new ConnectionPool().getConnection();
			connection.setAutoCommit(false);
			
			String sql = "insert into Produto (nome, descricao) values (? , ?)";
			stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, mesa.getNome());
			stmt.setString(2, mesa.getDescricao());
			
			result = stmt.execute();
			System.out.println("Inserir - Result:" + result);
						
			ResultSet rs = stmt.getGeneratedKeys();
			if(rs.next()){
				Integer id = Integer.parseInt(rs.getString("id")); 
				mesa.setId(id);
				
				System.out.println("Id:" + id + " Inserido no Banco !");
				
			}
						
			connection.commit();
		
		}catch(SQLException e){
			System.out.println("Erro ao inserir registro: ");
			e.printStackTrace();
			
			connection.rollback();
		}finally {
			System.out.println("Fechando conex�o.");
			stmt.close();
			connection.close();
		}

	}

}
